# gstlal-calibration

gstlal-calibration is a library that contains and supports a gstreamer-based pipeline 
used for the low-latency calibration of the LIGO detectors.  For more details about the
low-latency calibration procedure, see [this article](https://iopscience.iop.org/article/10.1088/1361-6382/aab658).

If you need assistance with installation please review INSTALL.md.

## Basic usage of the calibration pipeline
The low-latency calibration pipeline is found in the executable `gstlal_compute_strain`.
The pipeline requires a configuration file and a filters file.  An example configuration file can be found here:

[LHO configuration file](https://git.ligo.org/Calibration/ifo/H1/-/blob/main/gstlal_compute_strain_C00_H1.ini)

[LLO configuration file](https://git.ligo.org/Calibration/ifo/L1/-/blob/main/gstlal_compute_strain_C00_L1.ini)

The latest filters file in use for the low-latency calibration containing the FIR filters used by this pipeline can be found here:

[LHO filters file](https://ldas-jobs.ligo-wa.caltech.edu/~cal/archive/H1/reports/latest/gstlal_compute_strain_C00_filters_H1.npz)

[LLO filters file](https://ldas-jobs.ligo-la.caltech.edu/~cal/archive/L1/reports/latest/gstlal_compute_strain_C00_filters_L1.npz)

The pipeline can read input raw frames either from shared memory or from frame files on disk.
The pipeline can write the calibrated strain h(t) frames either to shared memory or to frame files on disk.

### Setting up the configuration
#### Reading from shared memory
In order to read from shared memory, the configuraiton file should contain the following options:
```shell
[InputConfigurations]
DataSource: lvshm
SHMPartition: partition_name (typically L[H,L]O_Online)
```

#### Reading from frame files on disk
In order to read from frame files on disk, the configuraiton file should contain the following options:
```shell
[InputConfigurations]
DataSource: frames
```
For this option, the frame file cache will have to be passed on the command line when running the pipeline.

#### Writing to shared memory
In order to write to shared memory, the configuraiton file should contain the following options:
```shell
[OutputConfigurations]
DataSink: lvshm
OutputSHMPartition: partition_name (for production this is L[H,L]O_hoft; testing should not write to this partition)
```

#### Writing to frame files
In order to write to frame files on disk, the configuration file should contain the following options:
```shell
[OutputConfigurations]
DataSink: frames
```

### Running the pipeline
#### Reading from shared memory
To run the low-latency calibration pipeline in a configuration where it is reading from shared memory, you can use the following:
```shell
gstlal_compute_strain --config-file $(CONFIGFILE) --filters-file $(FILTERSFILE)
```
where `CONFIGFILE` should be your configuration file as discussed above and `FILTERSFILE` should be be your `.npz` filters file containing the calibration FIR filters.

This command line will work if you are writing to frame files on disk or to shared memory.  If you are writing to frame files on disk and would like to specify a location to write these files, this can be done by adding the `--output-path` option.  Additionally, you can specify the duration of each frame and the number of frames in each file using the `--frame-duration` and `--frames-per-file` options, respectively.

### Reading from frame files on disk
To run the low-latency calibration pipeline in a configuration where it is reading from frame files on disk, you can use the following:
```shell
gstlal_compute_strain --config-file $(CONFIGFILE) --filters-file $(FILTERSFILE) ----gps-start-time $(START) --gps-end-time $(END) --frame-cache $(FRAMECACHE) --wings=$(WINGDURATION)
```
where `CONFIGFILE` should be your configuration file as discussed above, `FILTERSFILE` should be be your `.npz` filters file containing the calibration FIR filters, `START` and `END` should be your GPS start and end times, `FRAMECACHE` should be your raw frame file cache, and `WINGDURATION` should be the number of seconds you would like to trim as start up transient time from the written output.  It is recommended that this be at least a few minutes long since several processes in the calibration pipeline take a few minutes to settle

This command line will work if you are writing to frame files on disk or to shared memory.  If you are writing to frame files on disk and would like to specify a location to write these files, this can be done by adding the `--output-path` option.  Additionally, you can specify the duration of each frame and the number of frames in each file using the `--frame-duration` and `--frames-per-file` options, respectively.



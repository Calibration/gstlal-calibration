#!/usr/bin/env python3
# Copyright (C) 2022  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import matplotlib; matplotlib.use('Agg')
import sys
import os
import numpy as np
import time
from math import pi
import resource
import datetime
import time
import matplotlib.patches as mpatches
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 22
matplotlib.rcParams['legend.fontsize'] = 18
matplotlib.rcParams['mathtext.default'] = 'regular'
import glob
import matplotlib.pyplot as plt

from lal import LIGOTimeGPS
from ligo import segments

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
import test_common

from ticks_and_grid import ticks_and_grid

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject
from gi.repository import Gst
gi.require_version('GstController', '1.0')
from gi.repository import GstController
GObject.threads_init()
Gst.init(None)

#
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#

injection = "pc" # pc, T, P, or U
if injection == 'pc':
	channel_name = "CAL-PCALY_RX_PD_OUT_DQ"
	rate = 16384
	freq_update_function = calibration_parts.update_SS_demod_freqs
elif injection == 'T':
	channel_name = "SUS-ETMX_L3_LOCK_L_OUT"
	rate = 512
	freq_update_function = calibration_parts.update_SS_demod_freqs_T
elif injection == 'P':
	channel_name = "SUS-ETMX_L2_LOCK_L_OUT"
	rate = 512
	freq_update_function = calibration_parts.update_SS_demod_freqs_P
elif injection == 'U':
	channel_name = "SUS-ETMX_L1_LOCK_L_OUT"
	rate = 512
	freq_update_function = calibration_parts.update_SS_demod_freqs_U


def lal_find_linefreq_01(pipeline, name):
	#
	# This processes a swept-sine injection done with the Pcal.
	#

	channel_list = [("H1", channel_name), ("H1", "CAL-DARM_ERR_DBL_DQ")]

	# Get the data
	src = pipeparts.mklalcachesrc(pipeline, location = "H1_easy_raw_frames.cache", cache_dsc_regex = "H1", use_mmap = True)
	demux = pipeparts.mkframecppchanneldemux(pipeline, src, do_file_checksum = False, skip_bad_files = False, channel_list = list(map("%s:%s".__mod__, channel_list)))

	pcal = calibration_parts.hook_up(pipeline, demux, channel_name, "H1", 0.25)
	pcal = calibration_parts.caps_and_progress(pipeline, pcal, "audio/x-raw, format=F64LE, rate=%d, channel-mask=(bitmask)0x0" % rate, channel_name)
	pcal = pipeparts.mktee(pipeline, pcal)

	derr = calibration_parts.hook_up(pipeline, demux, "CAL-DARM_ERR_DBL_DQ", "H1", 0.25)
	derr = calibration_parts.caps_and_progress(pipeline, derr, "audio/x-raw, format=F64LE, rate=16384, channel-mask=(bitmask)0x0", "derr")
	derr = calibration_parts.mkresample(pipeline, derr, 5, False, rate)

	# Measure line frequency
	freq = calibration_parts.find_linefreq(pipeline, pcal, rate)

	# Record the frequency
	freq_data = calibration_parts.mkresample(pipeline, freq, 0, False, 16)
	freq_data = pipeparts.mkprogressreport(pipeline, freq_data, "line_freq")
	freq_data = pipeparts.mktee(pipeline, freq_data)
	pipeparts.mknxydumpsink(pipeline, freq_data, "%s_freq.txt" % channel_name)

	# Trick to make sure freq stays ahead of the demodulated data, needed for frequency tracking in lal_sweep
	# First, delay the no-longer-needed 16-Hz freq_data with a dummy filter.
	freq_data = calibration_parts.mkcomplexfirbank(pipeline, freq_data, latency = 80, fir_matrix = [np.zeros(160)])
	# Upsample
	freq_data = calibration_parts.mkresample(pipeline, freq_data, 0, False, 16384)
	# Interleave and deinterleave
	all_data = calibration_parts.mkinterleave(pipeline, [pcal, derr, freq_data])
	pcal, derr, freq_data = calibration_parts.mkdeinterleave(pipeline, all_data, 3)
	# Finally, get rid of freq_data
	pipeparts.mkfakesink(pipeline, freq_data)

	# Demodulate Pcal and d_err
	pcal_demod = pipeparts.mkgeneric(pipeline, pcal, "lal_demodulate")
	derr_demod = pipeparts.mkgeneric(pipeline, derr, "lal_demodulate")

	# Process swept-sine injection with lal_sweep
	R_data = calibration_parts.mkinterleave(pipeline, [pcal_demod, derr_demod], complex_data = True)
	# Keep it a few seconds behind the frequency data
	R_data = pipeparts.mkgeneric(pipeline, R_data, "queue", min_threshold_buffers = 10, min_threshold_time = int(5e9), min_threshold_bytes = 5 * rate * 2 * 16)
	R_data = pipeparts.mkprogressreport(pipeline, R_data, "sweep_data")
	sweep = pipeparts.mkgeneric(pipeline, R_data, "lal_sweep", filename = "%s_over_derr.txt" % channel_name)

	freq.connect("notify::timestamped-frequency", freq_update_function, pcal_demod, "timestamped_frequency", "line_frequency", 1)
	freq.connect("notify::timestamped-frequency", freq_update_function, derr_demod, "timestamped_frequency", "line_frequency", 1)
	freq.connect("notify::timestamped-frequency", freq_update_function, sweep, "timestamped_frequency", "frequency", 1)

	#
	# done
	#

	return pipeline

#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


test_common.build_and_run(lal_find_linefreq_01, "lal_find_linefreq_01")#, segment = segments.segment((LIGOTimeGPS(0, 1000000000 * (1268409984 + 9000)), LIGOTimeGPS(0, 1000000000 * (1268409984 + 11200)))))

# Get data
asd = np.loadtxt("asd.txt")
pcal_freq = np.loadtxt("%s_freq.txt" % channel_name)

# If there are multiple asds, choose the 2nd to last one
f = np.transpose(asd)[0]
asd = np.transpose(asd)[1]
idx = max(0, int(len(f) / (rate / 2 / f[1] + 1)) - 2)
f = f[int(idx * (rate / 2 / f[1] + 1)) : int((idx + 1) * (rate / 2 / f[1] + 1))]
asd = asd[int(idx * (rate / 2 / f[1] + 1)) : int((idx + 1) * (rate / 2 / f[1] + 1))]

t = np.transpose(pcal_freq)[0]
t0 = t[0]
t -= t0
pcal_freq = np.transpose(pcal_freq)[1]

filters = np.load('check_calibration/Filters/O3/GDSFilters/H1DCS_C01_1256655618_v2.npz')
R_model = filters['response_function']
R_model = R_model[1] + 1j * R_model[2]
pcal_corr = filters['y_arm_pcal_corr']
pcal_corr = pcal_corr[1] + 1j * pcal_corr[2]
R_model /= pcal_corr
R_measured = np.loadtxt('CAL-PCALY_RX_PD_OUT_DQ_over_derr.txt')
SS_freqs = np.transpose(R_measured)[0]
R_unc = np.transpose(R_measured)[3]
R_measured = np.transpose(R_measured)[1] + 1j * np.transpose(R_measured)[2]
meas_over_model = []
for i in range(len(SS_freqs)):
	idx = int(4 * SS_freqs[i])
	w2 = 4 * SS_freqs[i] - idx
	w1 = 1.0 - w2
	meas_over_model.append(R_measured[i] / (w1 * R_model[idx] + w2 * R_model[idx + 1]))

meas_over_model = np.array(meas_over_model) * np.exp(-2 * np.pi * 1j * SS_freqs / 16384)
meas_over_model_mag = abs(meas_over_model)
meas_over_model_phase = np.angle(meas_over_model) * 180 / np.pi

# Make plots
plt.figure(figsize = (10, 6))
plt.plot(f, asd, linewidth = 0.75)
plt.ylabel(r'${\rm ASD}\ \left[{\rm strain / }\sqrt{\rm Hz}\right]$')
plt.xlabel(r'${\rm Frequency \ [Hz]}$')
ticks_and_grid(plt.gca(), xmin = 10, xmax = rate / 2, ymin = 1e-20, ymax = 1e-12, xscale = 'log', yscale = 'log')
plt.savefig('pcal_asd_%d-%d.png' % (int(t0), int(t0 + t[-1])))
plt.close()

plt.figure(figsize = (10, 6))
plt.plot(t, pcal_freq, linewidth = 0.75)
plt.ylabel('Pcal Frequency [Hz]')
plt.xlabel('Time since %d [s]' % int(t0))
ticks_and_grid(plt.gca(), xmin = 1268420384 - t0, xmax = 1268421184 - t0, xscale = 'linear', yscale = 'log')
plt.savefig('pcal_freq_%d-%d.png' % (int(t0), int(t0 + t[-1])))
plt.close()

plt.figure(figsize = (15, 12))
plt.subplot(211)
plt.errorbar(SS_freqs, meas_over_model_mag, yerr = R_unc * meas_over_model_mag, ecolor = 'red', capsize = 4, mfc = 'red', mec = 'red', linestyle = 'None', marker = '.', markersize = 7.0)
plt.ylabel('Magnitude')
plt.title('H1 Pcal Swept Sine 2020-03-16 Response Function Measurement / Model')
ticks_and_grid(plt.gca(), yscale = 'linear', xscale = 'log', xmin = 5, xmax = 2000, ymin = 0.75, ymax = 1.25)
plt.subplot(212)
plt.errorbar(SS_freqs, meas_over_model_phase, yerr = R_unc * 180 / np.pi, ecolor = 'red', capsize = 4, mfc = 'red', mec = 'red', linestyle = 'None', marker = '.', markersize = 7.0)
plt.ylabel('Phase [deg]')
plt.xlabel("Frequency [Hz]")
ticks_and_grid(plt.gca(), yscale = 'linear', xscale = 'log', xmin = 5, xmax = 2000, ymin = -15, ymax = 15)
plt.savefig("H1_SS_20200316_meas_over_model_errorbars.png")
plt.close()



/*
 * Copyright (C) 2022  Aaron Viets
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


/**
 * SECTION:gstlalcalibration
 * @title: Misc
 * @include: gstlalcalibration.h
 * @short_description: Collection of miscellaneous utility functions.
 */


/*
 * ============================================================================
 *
 *				  Preamble
 *
 * ============================================================================
 */


/*
 * Stuff from the C library
 */


#include <math.h>
#include <stdio.h>


/*
 * stuff from GSL
 */


#include <gsl/gsl_matrix.h>



/*
 * Stuff from glib/GStreamer
 */


#include <glib.h>
#include <gst/gst.h>


/*
 * Our own stuff
 */


#include <gstlalcalibration.h>


/*
 * ============================================================================
 *
 *		 GSL Matrices and Vectors and GstValueArrays
 *
 * ============================================================================
 */


/**
 * gstlal_doubles_from_gst_value_array:
 * @va: the #GstValueArray from which to copy the elements
 * @dest:  address of memory large enough to hold elements, or %NULL
 * @n:  address of integer that will be set to the number of elements, or
 * %NULL
 *
 * Convert a #GstValueArray of doubles to an array of doubles.  If @dest is
 * %NULL then new memory will be allocated otherwise the doubles are copied
 * into the memory pointed to by @dest, which must be large enough to hold
 * them.  If memory is allocated by this function, free with g_free().  If
 * @n is not %NULL it is set to the number of elements in the array.
 *
 * Returns: @dest or the address of the newly-allocated memory on success,
 * or %NULL on failure.
 */


gdouble *gstlal_doubles_from_gst_value_array(GValue *va, gdouble *dest, gint *n)
{
	guint i;

	if(!va)
		return NULL;
	if(!dest)
		dest = g_new(gdouble, gst_value_array_get_size(va));
	if(!dest)
		return NULL;
	if(n)
		*n = gst_value_array_get_size(va);
	for(i = 0; i < gst_value_array_get_size(va); i++)
		dest[i] = g_value_get_double(gst_value_array_get_value(va, i));
	return dest;
}


/**
 * gstlal_gst_value_array_from_doubles:
 * @src:  start of array
 * @n: number of elements in array
 *
 * Build a #GstValueArray from an array of doubles.
 *
 * Returns: newly-allocated #GstValueArray
 */


GValue gstlal_gst_value_array_from_doubles(const gdouble *src, gint n)
{
	GValue va = G_VALUE_INIT;
	g_value_init(&va, GST_TYPE_ARRAY);
	GValue v = G_VALUE_INIT;
	g_value_init(&v, G_TYPE_DOUBLE);
	gint i;

	if(src) {
		for(i = 0; i < n; i++) {
			g_value_set_double(&v, src[i]);
			gst_value_array_append_value(&va, &v);
		}
	}
	return va;
}


/**
 * gstlal_gsl_matrix_from_gst_value_array:
 * @va:  #GstValueArray of #GstValueArrays of double
 *
 * Build a #gsl_matrix from a #GstValueArray of #GstValueArrays of doubles.
 *
 * Returns:  the newly-allocated #gsl_matrix or %NULL on failure.
 */


gsl_matrix *gstlal_gsl_matrix_from_gst_value_array(GValue *va)
{
	gsl_matrix *matrix;
	GValue row = G_VALUE_INIT;
	g_value_init(&row, GST_TYPE_ARRAY);
	guint rows, cols;
	guint i;

	if(!va)
		return NULL;
	rows = gst_value_array_get_size(va);
	if(!rows)
		/* 0x0 matrix */
		return gsl_matrix_alloc(0, 0);


	g_value_copy(gst_value_array_get_value(va, 0), &row);
	cols = gst_value_array_get_size(&row);
	matrix = gsl_matrix_alloc(rows, cols);
	if(!matrix)
		/* allocation failure */
		return NULL;
	if(!gstlal_doubles_from_gst_value_array(&row, gsl_matrix_ptr(matrix, 0, 0), NULL)) {
		/* row conversion failure */
		gsl_matrix_free(matrix);
		return NULL;
	}
	for(i = 1; i < rows; i++) {
		g_value_copy(gst_value_array_get_value(va, i), &row);
		if(gst_value_array_get_size(&row) != cols) {
			/* one of the rows has the wrong number of columns */
			gsl_matrix_free(matrix);
			return NULL;
		}
		if(!gstlal_doubles_from_gst_value_array(&row, gsl_matrix_ptr(matrix, i, 0), NULL)) {
			/* row conversion failure */
			gsl_matrix_free(matrix);
			return NULL;
		}
	}

	return matrix;
}


/**
 * gstlal_gst_value_array_from_gsl_matrix:
 * @matrix: a #gsl_matrix
 *
 * Build a #GstValueArray of #GstValueArrays of doubles from a #gsl_matrix.
 *
 * Returns:  the newly-allocated #GstValueArray of newly-allocated
 * #GstValueArrays of doubles
 */


GValue gstlal_gst_value_array_from_gsl_matrix(const gsl_matrix *matrix)
{
	GValue va = G_VALUE_INIT;
	g_value_init(&va, GST_TYPE_ARRAY);
	GValue v = G_VALUE_INIT;
	g_value_init(&v, GST_TYPE_ARRAY);
	guint i;

	for(i = 0; i < matrix->size1; i++) {
		v = gstlal_gst_value_array_from_doubles(gsl_matrix_const_ptr(matrix, i, 0), matrix->size2);
		gst_value_array_append_value(&va, &v);
	}
	return va;
}

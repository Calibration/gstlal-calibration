FROM containers.ligo.org/lscsoft/gstlal:gstlal-ugly-1.10.0-v1

# Labeling/packaging stuff:
LABEL name="GstLAL Calibration Runtime Package, RL8" \
      maintainer="Madeline Wade <madeline.wade@ligo.org>" \
      date="2022-11-16" \
      support="Reference Platform, RL8"

USER root

## Copy Optimized RPMs to container
COPY rpms /rpms

# Install Optimized RPMs, delete old RPMs
RUN yum makecache && \
	yum -y localinstall /rpms/*.rpm

# Update gstreamer registry cache:
ENV GST_REGISTRY_1_0=/usr/share/gstreamer-1.0/.registry.bin
RUN gst-inspect-1.0

# Clean up and close-out
RUN rm -rf /rpms && \
    yum clean all


# Export MKL environment variables: 
ENV MKL_INTERFACE_LAYER LP64
ENV MKL_THREADING_LAYER SEQUENTIAL
ENV TMPDIR /tmp

ENTRYPOINT bash

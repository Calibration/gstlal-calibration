
import numpy as np
import os


update_act_standard_files()
update_pcal_standard_files()
update_ASD_standard_files()
update_TDCF_standard_files()
update_STV_standard_files()


def update_act_standard_files():
	act_data_files = os.popen("ls act_data/*exc_0*.txt").read().split('\n')[:-1] + os.popen("ls act_data/*exc_1*.txt").read().split('\n')[:-1]
	for act_data_file in act_data_files:
		act_data_standard_file = act_data_file.split('exc')[0] + 'exc_standard' + act_data_file.split('exc')[1]
		act_data_standard = np.loadtxt(act_data_standard_file)
		act_data_standard_times = np.transpose(act_data_standard)[0]
		act_data = np.loadtxt(act_data_file)
		act_data_standard = np.zeros([len(act_data_standard), 3])
		i = 0
		successes = 0
		for j in range(len(act_data_standard_times)):
			while act_data_standard_times[j] != act_data[i][0] and i < len(act_data):
				i += 1
			if act_data_standard_times[j] == act_data[i][0]:
				act_data_standard[j] = act_data[i]
				successes += 1
		if successes != len(act_data_standard_times):
			print("%s: mismatch between data in new file and data in standard file.  Expected %d timestamp matches, found %d" % (act_data_file, len(act_data_standard_times), successes))
		else:
			os.system("rm %s" % act_data_standard_file)
			np.savetxt(act_data_standard_file, act_data_standard)


def update_pcal_standard_files():
	pcal_data_files = os.popen("ls pcal_data/*Pcal_0*.txt").read().split('\n')[:-1] + os.popen("ls pcal_data/*Pcal_1*.txt").read().split('\n')[:-1]
	for pcal_data_file in pcal_data_files:
		pcal_data_standard_file = pcal_data_file.split('Pcal')[0] + 'Pcal_standard' + pcal_data_file.split('Pcal')[1]
		pcal_data_standard = np.loadtxt(pcal_data_standard_file)
		pcal_data_standard_times = np.transpose(pcal_data_standard)[0]
		pcal_data = np.loadtxt(pcal_data_file)
		pcal_data_standard = np.zeros([len(pcal_data_standard), 3])
		i = 0
		successes = 0
		for j in range(len(pcal_data_standard_times)):
			while pcal_data_standard_times[j] != pcal_data[i][0] and i < len(pcal_data):
				i += 1
			if pcal_data_standard_times[j] == pcal_data[i][0]:
				pcal_data_standard[j] = pcal_data[i]
				successes += 1
		if successes != len(pcal_data_standard_times):
			print("%s: mismatch between data in new file and data in standard file.  Expected %d timestamp matches, found %d" % (pcal_data_file, len(pcal_data_standard_times), successes))
		else:
			os.system("rm %s" % pcal_data_standard_file)
			np.savetxt(pcal_data_standard_file, pcal_data_standard)


def update_ASD_standard_files():
	ASD_data_files = os.popen("ls ASD_data/*asd.txt").read().split('\n')[:-1]
	for ASD_data_file in ASD_data_files:
		ASD_standard_data_file = ASD_data_file.replace("asd.txt", "asd_standard.txt")
		ASD_data = np.loadtxt(ASD_data_file)
		ASD_standard_data = np.loadtxt(ASD_standard_data_file)
		if len(np.transpose(ASD_data)[0]) != len(np.transpose(ASD_standard_data)[0]):
			print("%s: mismatch in length of asd standard and new data files (%d != %d)" % (ASD_data_file, len(np.transpose(ASD_standard_data)[0]), len(np.transpose(ASD_data)[0])))
		elif (np.transpose(ASD_data)[0] != np.transpose(ASD_standard_data)[0]).any()
			print("%s: frequency vectors don't match" % ASD_data_file)
		else:
			os.system("rm %s" % ASD_standard_data_file)
			np.savetxt(ASD_standard_data_file, ASD_data)


def update_TDCF_standard_files():
	TDCF_data_files = os.popen("ls TDCF_data/*Approx.txt").read().split('\n')[:-1] + os.popen("ls TDCF_data/*Exact.txt").read().split('\n')[:-1]
	for TDCF_data_file in TDCF_data_files:
		TDCF_standard_data_file = TDCF_data_files.replace(".txt", "_standard.txt")
		TDCF_data_standard = np.loadtxt(TDCF_data_standard_file)
		TDCF_data_standard_times = np.transpose(TDCF_data_standard)[0]
		TDCF_data = np.loadtxt(TDCF_data_file)
		TDCF_data_standard = np.zeros([len(TDCF_data_standard), 2])
		i = 0
		successes = 0
		for j in range(len(TDCF_data_standard_times)):
			while TDCF_data_standard_times[j] != TDCF_data[i][0] and i < len(TDCF_data):
				i += 1
			if TDCF_data_standard_times[j] == TDCF_data[i][0]:
				TDCF_data_standard[j] = TDCF_data[i]
				successes += 1
		if successes != len(TDCF_data_standard_times):
			print("%s: mismatch between data in new file and data in standard file.  Expected %d timestamp matches, found %d" % (TDCF_data_file, len(TDCF_data_standard_times), successes))
		else:
			os.system("rm %s" % TDCF_data_standard_file)
			np.savetxt(TDCF_data_standard_file, TDCF_data_standard)


def update_STV_standard_files():
	STV_data_files = ['State_Vector_data/State_Vector_Approx.txt', 'State_Vector_data/State_Vector_Exact.txt']
	for STV_data_file in STV_data_files:
		STV_standard_data_file = STV_data_file.replace(".txt", "_standard.txt")
		STV_data = np.loadtxt(STV_data_file)
		STV_standard_data = np.loadtxt(STV_standard_data_file)
		if len(np.transpose(STV_data)[0]) != len(np.transpose(STV_standard_data)[0]):
			print("%s: mismatch in length of state vector standard and new data files (%d != %d)" % (STV_data_file, len(np.transpose(STV_standard_data)[0]), len(np.transpose(STV_data)[0])))
		elif (np.transpose(STV_data)[0] != np.transpose(STV_standard_data)[0]).any()
			print("%s: time vectors don't match" % STV_data_file)
		else:
			os.system("rm %s" % STV_standard_data_file)
			np.savetxt(STV_standard_data_file, STV_data)



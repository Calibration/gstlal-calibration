from gstlalcalibration import FIRtools

def test_find_prime_factors():
    prime_factors = FIRtools.find_prime_factors(10)
    assert prime_factors[0] == 2
    assert prime_factors[1] == 5
    assert prime_factors[2] == 1

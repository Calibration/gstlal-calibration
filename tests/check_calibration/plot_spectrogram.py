#!/usr/bin/env python3
#
# Copyright (C) 2024  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import matplotlib; matplotlib.use('Agg')
import numpy as np
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 22
matplotlib.rcParams['legend.fontsize'] = 18
matplotlib.rcParams['mathtext.default'] = 'regular'
import matplotlib.pyplot as plt
from matplotlib import ticker, cm
import matplotlib.gridspec as gridspec
import time
import datetime

from optparse import OptionParser, Option

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
Gst.init(None)

import lal
from lal import LIGOTimeGPS

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlal import simplehandler
from gstlal import datasource

from ligo import segments
from gstlalcalibration import test_common

from gstlalcalibration import FIRtools as fir
from ticks_and_grid import ticks_and_grid

parser = OptionParser()
parser.add_option("--gps-start-time", metavar = "seconds", type = int, help = "GPS time at which to start processing data")
parser.add_option("--gps-end-time", metavar = "seconds", type = int, help = "GPS time at which to stop processing data")
parser.add_option("--ifo", metavar = "name", type = str, help = "Name of the interferometer (IFO), e.g., H1, L1")
parser.add_option("--frame-cache", metavar = "name", type = str, help = "Frame cache file that contains data from which to compute the spectrogram")
parser.add_option("--channel-name", metavar = "name", type = str, default = None, help = "Names of channel that contains data from which to compute the spectrogram")
parser.add_option("--sample-rate", metavar = "Hz", type = str, default = '16384', help = "Sample rate of input data.")
parser.add_option("--fft-time", metavar = "seconds", type = float, default = 4, help = "Duration in seconds of FFTs used to compute spectrogram")
parser.add_option("--fft-spacing", metavar = "seconds", type = float, default = None, help = "Spacing in seconds of FFTs used to compute spectrogram.  Default is auto.")
parser.add_option("--window", type = int, default = 3, help = "Which window function to use to window the data when taking FFTs.  Options are 0 (DPSS), 1 (kaiser), 2 (Dolph-Chebyshev), 3 (Blackman, default), 4 (Hann), 5 (None)")
parser.add_option("--freq-res", metavar = "Hz", type = float, default = 1.0, help = "Frequency resolution of spectrogram.  Only applies when using Kaiser, DPSS, and Dolph Chebyshev windows.")
parser.add_option("--update-time", type = float, default = None, help = "Amount of time between consecutive ASDs on the spectrogram.  Default is auto.")
parser.add_option("--frequency-min", type = float, default = 10, help = "Minimum frequency for plot")
parser.add_option("--frequency-max", type = float, default = 8192, help = "Maximum frequency for plot")
parser.add_option("--ASD-min", type = float, default = 1e-24, help = "Minimum for ASD plot")
parser.add_option("--ASD-max", type = float, default = 1e-18, help = "Maximum for ASD plot")
parser.add_option("--amplitude-min", type = float, default = 0.01, help = "Minimum for amplitude on spectrogram plot")
parser.add_option("--amplitude-max", type = float, default = 100, help = "Maximum for amplitude on spectrogram plot")


options, filenames = parser.parse_args()

# Get frame cache list and channel list
ifo = options.ifo
frame_cache = options.frame_cache
channel = options.channel_name
chan_list = [(ifo, channel)]
sr = sample_rate = int(options.sample_rate)
update_time = options.update_time
dur = options.gps_end_time - options.gps_start_time
if update_time is None:
	# ~1000 samples is about right
	update_time = dur / 1000.0
	# Make it a power of 2
	update_time = 2**int(round(np.log2(update_time)))
	# It should be more than the fft_time
	while update_time < options.fft_time:
		update_time *= 2
fft_spacing = options.fft_spacing
if fft_spacing is None:
	fft_spacing = update_time / 8
	while fft_spacing < options.fft_time / 4 and fft_spacing < update_time:
		fft_spacing *= 2

# 
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#


#
# This pipeline reads in time series data and writes it to file.
#


def compute_spectrogram(pipeline, name):

	data = pipeparts.mklalcachesrc(pipeline, location = frame_cache, cache_dsc_regex = ifo)
	data = pipeparts.mkframecppchanneldemux(pipeline, data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, chan_list)))
	data = calibration_parts.hook_up(pipeline, data, channel, ifo, 1.0)
	data = calibration_parts.caps_and_progress(pipeline, data, "audio/x-raw,format=F64LE", channel)
	data = calibration_parts.mkresample(pipeline, data, 5, False, sr)
	data = pipeparts.mktee(pipeline, data)
	# Compute ASDs for the spectrogram
	pipeparts.mkgeneric(pipeline, data, "lal_asd", fft_samples = int(options.fft_time * sr), overlap_samples = int((options.fft_time - fft_spacing) * sr), window_type = int(options.window), frequency_resolution = float(options.freq_res), update_time = update_time, use_td_median = True, filename = '%s_%s_spectrogram_%d-%d.txt' % (ifo, channel, options.gps_start_time, dur))
	# Compute one median ASD to normalize the above ASDs
	pipeparts.mkgeneric(pipeline, data, "lal_asd", fft_samples = int(options.fft_time * sr), overlap_samples = int((options.fft_time - fft_spacing) * sr), window_type = int(options.window), frequency_resolution = float(options.freq_res), update_time = 2 * int(dur), use_td_median = True, filename = '%s_%s_ASD_%d-%d.txt' % (ifo, channel, options.gps_start_time, dur))

	#
	# done
	#

	return pipeline

#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


# Run pipeline
test_common.build_and_run(compute_spectrogram, "compute_spectrogram", segment = segments.segment((LIGOTimeGPS(0, 1000000000 * options.gps_start_time), LIGOTimeGPS(0, 1000000000 * options.gps_end_time))))

# Unnormalized spectrogram data
spec =  np.loadtxt('%s_%s_spectrogram_%d-%d.txt' % (ifo, channel, options.gps_start_time, dur))

# ASD to normalize it
asd = np.loadtxt('%s_%s_ASD_%d-%d.txt' % (ifo, channel, options.gps_start_time, dur))

# Frequency vector
fvec = np.arange(0, sr / 2.0 + sr / 4.0 / len(asd), sr / 2.0 / (len(asd) - 1))

# Time vector
num_t = len(spec) // len(asd)
max_t = update_time * (num_t - 1)
tvec = np.arange(0, max_t + update_time / 2, update_time)

t0 = options.gps_start_time + update_time / 2

# Normalize spectrogram
asd = np.transpose(asd)[1]
spec = np.transpose(spec)[1]
spec2D = np.ones([len(tvec), len(fvec)])
for i in range(num_t):
	spec2D[i] = spec[i * len(asd) : (i + 1) * len(asd)] / asd

# Units of time to display on horizontal axis of spectrogram
t_unit = 'seconds'
sec_per_t_unit = 1.0
if max_t > 60 * 60 * 100:
	t_unit = 'days'
	sec_per_t_unit = 60.0 * 60.0 * 24.0
elif max_t > 60 * 100:
	t_unit = 'hours'
	sec_per_t_unit = 60.0 * 60.0
elif max_t > 100:
	t_unit = 'minutes'
	sec_per_t_unit = 60.0

tvec /= sec_per_t_unit

# Make plots
freq_scale = 'log' if options.frequency_min > 0.0 and options.frequency_max / options.frequency_min > 10 else 'linear'
ASD_scale = 'log' if options.ASD_min > 0.0 and options.ASD_max / options.ASD_min > 10 else 'linear'

# Spectrogram
fig = plt.figure(figsize = (15, 9))
ax = plt.gca()
X, Y = np.meshgrid(tvec, fvec)
Z = np.transpose(spec2D)
CS = ax.pcolor(X, Y, Z, norm=matplotlib.colors.LogNorm(vmin = float(options.amplitude_min), vmax = float(options.amplitude_max)), cmap = cm.Spectral_r)
fig.colorbar(CS, label = "Amplitude relative to median")
plt.xlabel(r'${\rm Time \ [%s] \  from \ %s \  UTC \ (%d)}$' % (t_unit, time.strftime("%b %d %Y %H:%M:%S".replace(':', '{:}').replace('-', '\mbox{-}').replace(' ', '\ '), time.gmtime(t0 + 315964782)), t0))
plt.ylabel(r'${\rm Frequency \ [Hz]}$')
plt.title(r'${\rm %s:%s \ Spectrogram}$' % (ifo, channel.replace('_', '\_')))

ticks_and_grid(ax, ymin = options.frequency_min, ymax = options.frequency_max, yscale = freq_scale, xscale = 'linear')

plt.savefig('%s_%s_spectrogram_%d-%dHz_%d-%d.png' % (ifo, channel, int(options.frequency_min), int(options.frequency_max), options.gps_start_time, dur))
plt.close()

# Median ASD for reference
plt.figure(figsize = (10, 6))
plt.plot(fvec, asd, linewidth = 0.75)
plt.title(r'${\rm %s:%s ASD}$' % (ifo, channel.replace('_', '\_')))
plt.ylabel(r'${\rm ASD}\ \left[{\rm strain / }\sqrt{\rm Hz}\right]$')
plt.xlabel(r'${\rm Frequency \ [Hz]}$')
ticks_and_grid(plt.gca(), xmin = options.frequency_min, xmax = options.frequency_max, ymin = options.ASD_min, ymax = options.ASD_max, xscale = freq_scale, yscale = ASD_scale)
plt.tight_layout()

plt.savefig('%s_%s_ASD_%d-%dHz_%d-%d.png' % (ifo, channel, int(options.frequency_min), int(options.frequency_max), options.gps_start_time, dur))



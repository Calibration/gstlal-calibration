# Copyright (C) 2018  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import os
import numpy
from math import pi
from math import erf
import resource
import datetime
import glob

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
Gst.init(None)

import lal
from lal import LIGOTimeGPS

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlal import simplehandler
from gstlal import datasource

from ligo import segments
from gstlalcalibration import test_common

from tests.tests_pytest.error import rms
from tests.tests_pytest.plot import plot_deltal_over_inj_timeseries


ifo = 'H1'


#
# Load in the filters file that contains filter coefficients, etc.
#

filters = numpy.load("tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1_20240330T211519Z.npz")

# Set up gstlal frame cache list
gstlal_frame_cache_list = ['tests/tests_pytest/GDS_Approx_frames.cache', 'tests/tests_pytest/GDS_Exact_frames.cache']
gstlal_channels = ['GDS-CALIB_STRAIN', 'GDS-CALIB_STRAIN']

# Set list of all channels
channel_list = []
act_channels = ['SUS-ETMX_L3_CAL_LINE_OUT_DQ', 'SUS-ETMX_L2_CAL_LINE_OUT_DQ', 'SUS-ETMX_L1_CAL_LINE_OUT_DQ']
act_names = ['TST_exc', 'PUM_exc', 'UIM_exc']
TDCF_channels = ['GDS-CALIB_KAPPA_TST_REAL', 'GDS-CALIB_KAPPA_TST_IMAGINARY', 'GDS-CALIB_KAPPA_PUM_REAL', 'GDS-CALIB_KAPPA_PUM_IMAGINARY', 'GDS-CALIB_KAPPA_UIM_REAL', 'GDS-CALIB_KAPPA_UIM_IMAGINARY']
for channel in act_channels:
	channel_list.append((ifo, channel))
for channel in gstlal_channels:
	channel_list.append((ifo, channel))
for channel in TDCF_channels:
	channel_list.append((ifo, channel))

# Read stuff we need from the filters file
frequencies = []
act_corrections = []

act_line_names = ['ktst_esd', 'pum_act', 'uim_act']
EPICS = ['AT0_fT', 'AP0_fP', 'AU0_fU']
for i in range(len(act_line_names)):
	frequencies.append(float(filters["%s_line_freq" % act_line_names[i]]))
	act_corrections.append(float(filters["%s_re" % EPICS[i]]))
	act_corrections.append(float(filters["%s_im" % EPICS[i]]))

act_time_advance = 0.00006103515625
for i in range(0, len(act_corrections) // 2):
	corr = act_corrections[2 * i] + 1j * act_corrections[2 * i + 1]
	corr *= numpy.exp(2.0 * numpy.pi * 1j * frequencies[i] * act_time_advance)
	act_corrections[2 * i] = numpy.real(corr)
	act_corrections[2 * i + 1] = numpy.imag(corr)

demodulated_act_list = []
try:
	arm_length = float(filters['arm_length'])
except:
	arm_length = 3995.1

# demodulation and averaging parameters
filter_time = 20
median_time = 128
rate_out = 1

#
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#


def act2darm(pipeline, name):

	# Get actuation injection channels from the raw frames
	act_inj_channels = []
	raw_data = pipeparts.mklalcachesrc(pipeline, location = "tests/tests_pytest/raw_frames.cache", cache_dsc_regex = ifo)
	raw_data = pipeparts.mkframecppchanneldemux(pipeline, raw_data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, channel_list)))
	for i in range(len(act_channels)):
		act_inj_channels.append(calibration_parts.hook_up(pipeline, raw_data, act_channels[i], ifo, 1.0))
		act_inj_channels[i] = calibration_parts.caps_and_progress(pipeline, act_inj_channels[i], "audio/x-raw,format=F64LE,channels=1,channel-mask=(bitmask)0x0", "act_inj_%d" % i)
		act_inj_channels[i] = pipeparts.mktee(pipeline, act_inj_channels[i])

	# Demodulate the actuation injection channels at the lines of interest
	for i in range(len(frequencies)):
		demodulated_act = calibration_parts.demodulate(pipeline, act_inj_channels[i], frequencies[i], True, rate_out, filter_time, 0.5, prefactor_real = act_corrections[2 * i], prefactor_imag = act_corrections[2 * i + 1])
		demodulated_act_list.append(pipeparts.mktee(pipeline, demodulated_act))

	cache_num = 0
	for cache, channel, in zip(gstlal_frame_cache_list, gstlal_channels):
		version = "Approx" if "Approx" in cache else "Exact"
		# Get gstlal channels from the gstlal frames
		hoft_data = pipeparts.mklalcachesrc(pipeline, location = cache, cache_dsc_regex = ifo)
		hoft_data = pipeparts.mkframecppchanneldemux(pipeline, hoft_data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, channel_list)))
		hoft = calibration_parts.hook_up(pipeline, hoft_data, channel, ifo, 1.0, element_name_suffix = "_%d" % cache_num)
		hoft = calibration_parts.caps_and_progress(pipeline, hoft, "audio/x-raw,format=F64LE,channels=1,channel-mask=(bitmask)0x0", "%s_%d" % (channel, cache_num))
		deltal = pipeparts.mkaudioamplify(pipeline, hoft, arm_length)
		deltal = pipeparts.mktee(pipeline, deltal)

		for i in range(len(frequencies)):
			# Demodulate \DeltaL at each line
			demodulated_deltal = calibration_parts.demodulate(pipeline, deltal, frequencies[i], True, rate_out, filter_time, 0.5)

			# Divide by a TDCF if needed
			if 'tst' in act_line_names[i] or 'esd' in act_line_names[i]:
				# Get KAPPA_TST
				TDCF_real = "GDS-CALIB_KAPPA_TST_REAL"
				TDCF_imag = "GDS-CALIB_KAPPA_TST_IMAGINARY"
				TDCF_real = calibration_parts.hook_up(pipeline, hoft_data, TDCF_real, ifo, 1.0, element_name_suffix = "_%d" % cache_num)
				TDCF_imag = calibration_parts.hook_up(pipeline, hoft_data, TDCF_imag, ifo, 1.0, element_name_suffix = "_%d" % cache_num)
				TDCF = calibration_parts.merge_into_complex(pipeline, TDCF_real, TDCF_imag)
				TDCF = calibration_parts.caps_and_progress(pipeline, TDCF, "audio/x-raw,format=Z128LE,channels=1,channel-mask=(bitmask)0x0", 'KAPPA_TST_%s' % version)
				TDCF = calibration_parts.mkresample(pipeline, TDCF, 3, False, rate_out)
				if "Approx" in cache:
					# Only divide by the magnitude
					TDCF = pipeparts.mkgeneric(pipeline, TDCF, "cabs")
					TDCF = pipeparts.mktogglecomplex(pipeline, calibration_parts.mkmatmix(pipeline, TDCF, matrix = [[1.0, 0.0]]))
				demodulated_deltal = calibration_parts.complex_division(pipeline, demodulated_deltal, TDCF)
			elif 'pum' in act_line_names[i]:
				# Get KAPPA_PUM
				TDCF_real = "GDS-CALIB_KAPPA_PUM_REAL"
				TDCF_imag = "GDS-CALIB_KAPPA_PUM_IMAGINARY"
				TDCF_real = calibration_parts.hook_up(pipeline, hoft_data, TDCF_real, ifo, 1.0, element_name_suffix = "_%d" % cache_num)
				TDCF_imag = calibration_parts.hook_up(pipeline, hoft_data, TDCF_imag, ifo, 1.0, element_name_suffix = "_%d" % cache_num)
				TDCF = calibration_parts.merge_into_complex(pipeline, TDCF_real, TDCF_imag)
				TDCF = calibration_parts.caps_and_progress(pipeline, TDCF, "audio/x-raw,format=Z128LE,channels=1,channel-mask=(bitmask)0x0", 'KAPPA_PUM_%s' % version)
				TDCF = calibration_parts.mkresample(pipeline, TDCF, 3, False, rate_out)
				if "Approx" in cache:
					# Only divide by the magnitude
					TDCF = pipeparts.mkgeneric(pipeline, TDCF, "cabs")
					TDCF = pipeparts.mktogglecomplex(pipeline, calibration_parts.mkmatmix(pipeline, TDCF, matrix = [[1.0, 0.0]]))
				demodulated_deltal = calibration_parts.complex_division(pipeline, demodulated_deltal, TDCF)
			elif 'uim' in act_line_names[i]:
				# Get KAPPA_UIM
				TDCF_real = "GDS-CALIB_KAPPA_UIM_REAL"
				TDCF_imag = "GDS-CALIB_KAPPA_UIM_IMAGINARY"
				TDCF_real = calibration_parts.hook_up(pipeline, hoft_data, TDCF_real, ifo, 1.0, element_name_suffix = "_%d" % cache_num)
				TDCF_imag = calibration_parts.hook_up(pipeline, hoft_data, TDCF_imag, ifo, 1.0, element_name_suffix = "_%d" % cache_num)
				TDCF = calibration_parts.merge_into_complex(pipeline, TDCF_real, TDCF_imag)
				TDCF = calibration_parts.caps_and_progress(pipeline, TDCF, "audio/x-raw,format=Z128LE,channels=1,channel-mask=(bitmask)0x0", 'KAPPA_UIM_%s' % version)
				TDCF = calibration_parts.mkresample(pipeline, TDCF, 3, False, rate_out)
				if "Approx" in cache:
					# Only divide by the magnitude
					TDCF = pipeparts.mkgeneric(pipeline, TDCF, "cabs")
					TDCF = pipeparts.mktogglecomplex(pipeline, calibration_parts.mkmatmix(pipeline, TDCF, matrix = [[1.0, 0.0]]))
				demodulated_deltal = calibration_parts.complex_division(pipeline, demodulated_deltal, TDCF)

			# Take ratio \DeltaL(f) / act(f)
			deltaL_over_act = calibration_parts.complex_division(pipeline, demodulated_deltal, demodulated_act_list[i])
			# Take a running median
			deltaL_over_act = pipeparts.mkgeneric(pipeline, deltaL_over_act, "lal_smoothkappas", array_size = int(rate_out * median_time), no_default = True, filter_latency = 0.5)
			# The first samples are not median'ed.  Remove only half, since sometimes early data is important.
			deltaL_over_act = calibration_parts.mkinsertgap(pipeline, deltaL_over_act, insert_gap = False, chop_length = 500000000 * median_time)
			# Find the magnitude
			deltaL_over_act = pipeparts.mktee(pipeline, deltaL_over_act)
			magnitude = pipeparts.mkgeneric(pipeline, deltaL_over_act, "cabs")
			# Find the phase
			phase = pipeparts.mkgeneric(pipeline, deltaL_over_act, "carg")
			phase = pipeparts.mkaudioamplify(pipeline, phase, 180.0 / numpy.pi)
			# Interleave
			magnitude_and_phase = calibration_parts.mkinterleave(pipeline, [magnitude, phase])
			magnitude_and_phase = pipeparts.mkprogressreport(pipeline, magnitude_and_phase, name = "progress_sink_%s_%d_%d" % (channel, cache_num, i))
			# Write to file
			pipeparts.mknxydumpsink(pipeline, magnitude_and_phase, "tests/tests_pytest/act_data/%s_%s_over_%s_%d_at_%0.1fHz.txt" % (ifo, channel.replace(' ', '_'), act_names[i], cache_num, frequencies[i]))
		cache_num = cache_num + 1

	#
	# done
	#

	return pipeline

#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


def Act2darm():
	gps_start_time = int(os.popen("ls tests/tests_pytest/frames/GDS/H-H1GDS_Approx-*-4.gwf").read().split("\n")[0].split('-')[2])
	gps_end_time = 4 + int(os.popen("ls tests/tests_pytest/frames/GDS/H-H1GDS_Approx-*-4.gwf").read().split("\n")[-2].split('-')[2])
	test_common.build_and_run(act2darm, "act2darm", segment = segments.segment((LIGOTimeGPS(0, 1000000000 * gps_start_time), LIGOTimeGPS(0, 1000000000 * gps_end_time))))
	plot_deltal_over_inj_timeseries('act')
	rms('A')
	rms('A', 'E')


# Copyright (C) 2023  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import numpy as np
from gstlalcalibration import calibration_parts
from gstlal import pipeparts
from gstlalcalibration import test_common

from tests.tests_pytest.error import rms
from tests.tests_pytest.plot import plot_ASD

from lal import LIGOTimeGPS


ifo = 'H1'

def compute_ASD_from_Approx_cache(pipeline, name):

	data = pipeparts.mklalcachesrc(pipeline, location = "tests/tests_pytest/GDS_Approx_frames.cache", cache_dsc_regex = ifo)
	data = pipeparts.mkframecppchanneldemux(pipeline, data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, [(ifo, "GDS-CALIB_STRAIN"), (ifo, "GDS-CALIB_STRAIN_CLEAN")])))
	hoft = calibration_parts.hook_up(pipeline, data, "GDS-CALIB_STRAIN", ifo, 1.0, element_name_suffix = "0")
	#hoft = pipeparts.mktee(pipeline, hoft)
	#pipeparts.mknxydumpsink(pipeline, hoft, "tests/tests_pytest/ASD_data/Approx_hoft.txt")
	clean = calibration_parts.hook_up(pipeline, data, "GDS-CALIB_STRAIN_CLEAN", ifo, 1.0, element_name_suffix = "1")
	#clean = pipeparts.mktee(pipeline, clean)
	#pipeparts.mknxydumpsink(pipeline, clean, "tests/tests_pytest/ASD_data/Approx_clean.txt")
	hoft = pipeparts.mkprogressreport(pipeline, hoft, "hoft")
	clean = pipeparts.mkprogressreport(pipeline, clean, "clean")
	pipeparts.mkgeneric(pipeline, hoft, "lal_asd", filename = "tests/tests_pytest/ASD_data/Approx_hoft_asd.txt", fft_samples = 65536, overlap_samples = 32768, use_td_median = True, update_time = 3000)
	pipeparts.mkgeneric(pipeline, clean, "lal_asd", filename = "tests/tests_pytest/ASD_data/Approx_clean_asd.txt", fft_samples = 65536, overlap_samples = 32768, use_td_median = True, update_time = 3000)

	#
	# done
	#

	return pipeline


def compute_ASD_from_Exact_cache(pipeline, name):

	data = pipeparts.mklalcachesrc(pipeline, location = "tests/tests_pytest/GDS_Exact_frames.cache", cache_dsc_regex = ifo)
	data = pipeparts.mkframecppchanneldemux(pipeline, data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, [(ifo, "GDS-CALIB_STRAIN"), (ifo, "GDS-CALIB_STRAIN_CLEAN")])))
	hoft = calibration_parts.hook_up(pipeline, data, "GDS-CALIB_STRAIN", ifo, 1.0, element_name_suffix = "0")
	#hoft = pipeparts.mktee(pipeline, hoft)
	#pipeparts.mknxydumpsink(pipeline, hoft, "tests/tests_pytest/ASD_data/Exact_hoft.txt")
	clean = calibration_parts.hook_up(pipeline, data, "GDS-CALIB_STRAIN_CLEAN", ifo, 1.0, element_name_suffix = "1")
	#clean = pipeparts.mktee(pipeline, clean)
	#pipeparts.mknxydumpsink(pipeline, clean, "tests/tests_pytest/ASD_data/Exact_clean.txt")
	hoft = pipeparts.mkprogressreport(pipeline, hoft, "hoft")
	clean = pipeparts.mkprogressreport(pipeline, clean, "clean")
	pipeparts.mkgeneric(pipeline, hoft, "lal_asd", filename = "tests/tests_pytest/ASD_data/Exact_hoft_asd.txt", fft_samples = 65536, overlap_samples = 32768, use_td_median = True, update_time = 3000)
	pipeparts.mkgeneric(pipeline, clean, "lal_asd", filename = "tests/tests_pytest/ASD_data/Exact_clean_asd.txt", fft_samples = 65536, overlap_samples = 32768, use_td_median = True, update_time = 3000)

	#
	# done
	#

	return pipeline


def ASD():
	test_common.build_and_run(compute_ASD_from_Approx_cache, "compute_ASD_from_Approx_cache")
	test_common.build_and_run(compute_ASD_from_Exact_cache, "compute_ASD_from_Exact_cache")
	plot_ASD(hoft_f = 'Approx_hoft_asd.txt', clean_f = 'Approx_clean_asd.txt')
	plot_ASD(hoft_f = 'Exact_hoft_asd.txt', clean_f = 'Exact_clean_asd.txt')
	plot_ASD(hoft_f = 'Approx_hoft_asd.txt', clean_f = 'Approx_clean_asd.txt', standard = 'Approx_hoft_asd_standard.txt')
	plot_ASD(hoft_f = 'Exact_hoft_asd.txt', clean_f = 'Exact_clean_asd.txt', standard = 'Exact_hoft_asd_standard.txt')
	rms('H')
	rms('C')

